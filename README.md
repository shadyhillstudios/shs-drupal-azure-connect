# README #

# Requirements
This module will install OpenID Connect and the Azure AD extension to allow authentication against
the UMass Application.

# Purpose
Currently two patches: 
    1) update the App version paramaters requested for tokens
    2) force the UPN as a Drupal user's email

# Long term
Eventually the goal is to write a UMass custom module to allow full control over the Auth sequence
and data structure.